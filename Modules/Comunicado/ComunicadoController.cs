﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirebaseAdmin;
using FirebaseAdmin.Messaging;
using guarderiaApi.dto;
using guarderiaApi.models;
using guarderiaApi.repositories;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace guarderiaApi.controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComunicadoController : ControllerBase
    {
        private ComunicadoRepository comunicadoRepository;

        public ComunicadoController(ComunicadoRepository comunicadoRepository)
        {
            this.comunicadoRepository = comunicadoRepository;
        }

        // GET api/comunicado
        [HttpGet]
        public async Task<ActionResult<List<GetComunicadoDto>>> Get()
        {
            return await comunicadoRepository.findAll();
        }

        // GET api/comunicado/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Comunicado>> Get(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return BadRequest();
            }
            var comunicado = await comunicadoRepository.findById(id);
            return comunicado;
        }

        // Post api/comunicado
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] CreateComunicadoDto comunicado)
        {
            var creado = await comunicadoRepository.create(comunicado);
            if (creado)
            {
                return Ok();
            }
            else
            {
                return Conflict();
            }
        }

        // PUT api/comunicado/5
        [HttpPut]
        public async Task<ActionResult> Put([FromBody] UpdateComunicadoDto value)
        {
            var modificado = await comunicadoRepository.update(value);
            if (modificado)
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }

        // Post api/comunicado/enterado
        [HttpGet("enterado/{id}")]
        public async Task<ActionResult> Get(int id)
        {
            var enterado = await comunicadoRepository.enterado(id);
            if (enterado)
            {
                return Ok(new Comunicado());
            }
            else
            {
                return Conflict();
            }
        }
    }
}
