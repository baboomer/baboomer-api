using System;
using Dapper.Contrib.Extensions;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace guarderiaApi.models
{
    [Table("Comunicado")]
    public class Comunicado
    {
        [Key]
        public int Id { get; set; }
        public string Mensaje { get; set; }
        public string AutorComunicadoId { get; set; }
        public string Imagen { get; set; }
        public int TipoComunicado { get; set; }
        public int TutorId { get; set; }
        public bool Enterado { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}

