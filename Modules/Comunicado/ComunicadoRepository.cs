using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using Dapper.Contrib.Extensions;
using FirebaseAdmin.Messaging;
using guarderiaApi.dto;
using guarderiaApi.models;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;

namespace guarderiaApi.repositories
{
    public interface IComunicadoRepository
    {
        Task<List<GetComunicadoDto>> findAll();
        Task<bool> create(CreateComunicadoDto model);
        Task<Comunicado> findById(string id);
        Task<bool> update(UpdateComunicadoDto model);
        Task<bool> enterado(int id);
    }

    public class ComunicadoRepository : IComunicadoRepository
    {
        private readonly IDbConnection conn;
        private readonly TutorRepository tutorRepository;

        public ComunicadoRepository(string connectionString, TutorRepository tutorRepository)
        {
            conn = new MySqlConnection(connectionString);
            this.tutorRepository = tutorRepository;
        }

        public async Task<List<GetComunicadoDto>> findAll()
        {
            var sql = "SELECT I.*, T.* FROM Comunicado I" +
            " INNER JOIN Usuario T on I.AutorComunicadoId = T.Id"+
            " WHERE I.Enterado = 0"+
            " ORDER BY I.TipoComunicado DESC, I.CreatedAt DESC";
            var result = await conn.QueryAsync<GetComunicadoDto, Usuario, GetComunicadoDto>(sql,
            (i, t) => {
                i.AutorComunicado = t; 
                i.Fecha = i.CreatedAt.ToString("dd/MM/yyyy hh:mm:ss tt");
                return i;
            }, splitOn: "Id");

            return result.AsList();
        }

        public async Task<bool> create(CreateComunicadoDto comunicado)
        {
            var id = await conn.InsertAsync(comunicado);
            return await enviarNotificacion(comunicado.TipoComunicado, comunicado.Mensaje, comunicado.TutorId);
        }

        public async Task<Comunicado> findById(string id)
        {
            var video = await conn.GetAsync<Comunicado>(id);
            return video;
        }

        public async Task<bool> update(UpdateComunicadoDto model)
        {
            return await conn.UpdateAsync(model);
        }
        public async Task<bool> enterado(int id)
        {
            string UPDATE_COMUNICADO = $"UPDATE Comunicado SET Enterado='{1}' WHERE Id='{id}';";
            await conn.QueryAsync(UPDATE_COMUNICADO);
            return true;
        }

      public async Task<bool> enviarNotificacion(int tipoComunicado, string mensaje, string tutorId)
       {
           var registrationTokens = new List<string>();
           var title = "";
           if(tipoComunicado == 0)
           {
               title = "Comunicado";
               var tutores = await tutorRepository.findAll();
               foreach (var tutor in tutores)
               {
                   registrationTokens.Add(tutor.TokenFirebase);
               }
           } else {
               title = "Aviso importante";
               var tutor = await tutorRepository.findById(tutorId);
               registrationTokens.Add(tutor.TokenFirebase);
           }
            var message = new MulticastMessage()
            {
                Tokens = registrationTokens,
                Notification = new Notification()
                {
                    Title = title,
                    Body = mensaje,
                },
            };
            try
            {
                var response = await FirebaseMessaging.DefaultInstance.SendMulticastAsync(message);
                if (response.FailureCount > 0)
                {
                    var failedTokens = new List<string>();
                    for (var i = 0; i < response.Responses.Count; i++)
                    {
                        if (!response.Responses[i].IsSuccess)
                        {
                            failedTokens.Add(registrationTokens[i]);
                        }
                    }
                    Console.WriteLine($"List of tokens that caused failures: {failedTokens}");
                    return false;
                }
            }
            catch (Exception error)
            {
                Console.WriteLine("Error en firebase: " + error);
            }
            return true;
        }

        private void Ok(string v)
        {
            throw new NotImplementedException();
        }

        private void BadRequest(string v)
        {
            throw new NotImplementedException();
        }
    }
}