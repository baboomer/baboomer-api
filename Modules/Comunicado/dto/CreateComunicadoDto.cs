using System;
using System.ComponentModel.DataAnnotations;
using Dapper.Contrib.Extensions;
using guarderiaApi.models;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace guarderiaApi.dto
{
    [Table("Comunicado")]
    public class CreateComunicadoDto
    {   
        [Required]
        public int AutorComunicadoId { get; set; }
        [Required]
        public string Mensaje { get; set; }
        public string Imagen { get; set; }
        public int TipoComunicado { get; set; }
        public string TutorId { get; set; }
        public bool enterado { get; set; }
    }
}