using System;
using System.ComponentModel.DataAnnotations;
using Dapper.Contrib.Extensions;
using guarderiaApi.models;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace guarderiaApi.dto
{
    public class GetComunicadoDto
    {   
        public int Id { get; set; }
        public Usuario AutorComunicado { get; set; }
        public string Mensaje { get; set; }
        public string Fecha { get; set; }
        public string Imagen { get; set; }
        public int TipoComunicado { get; set; }
        public bool Enterado { get; set; } 
        public DateTime CreatedAt { get; set; }   
    }
}