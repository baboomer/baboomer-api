using System;
using System.ComponentModel.DataAnnotations;
using Dapper.Contrib.Extensions;
using guarderiaApi.models;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace guarderiaApi.dto
{
    [Table("Comunicado")]
    public class UpdateComunicadoDto
    {   
        [Required]
        public int Id { get; set; }
        public Usuario AutorComunicado { get; set; }
        public string Mensaje { get; set; }
        public DateTime Fecha { get; set; }
        public string Imagen { get; set; }
        public int TipoComunicado { get; set; }
        public int Enterado { get; set; }
    }
}