﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirebaseAdmin;
using FirebaseAdmin.Messaging;
using guarderiaApi.dto;
using guarderiaApi.models;
using guarderiaApi.repositories;
using Microsoft.AspNetCore.Mvc;

namespace guarderiaApi.controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InfanteController : ControllerBase
    {
        private InfanteRepository infanteRepository;

        public InfanteController(InfanteRepository infanteRepository)
        {
            this.infanteRepository = infanteRepository;
        }

        // GET api/infante
        [HttpGet]
        public async Task<ActionResult<List<GetInfanteDto>>> Get()
        {
            return await infanteRepository.findAll();
        }

        // GET api/infante/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Infante>> Get(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return BadRequest();
            }
            var infante = await infanteRepository.findById(id);
            return infante;
        }

        // Post api/infante
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateInfanteDto infante)
        {
            var creado = await infanteRepository.create(infante);
            if (creado)
            {
                return Ok();
            }
            else
            {
                return Conflict("Ya existe un tutor registrado con ese email");
            }
        }

        // PUT api/infante/5
        [HttpPut]
        public async Task<ActionResult> Put([FromBody] UpdateInfanteDto value)
        {
            var modificado = await infanteRepository.update(value);
            if (modificado)
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }

        // DELETE api/infante/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var borrado = await infanteRepository.delete(id);
            if (borrado)
            {
                return Ok();
            }
            else
            {
                return Conflict();
            }
        }
    }
}
