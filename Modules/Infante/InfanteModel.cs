using System;
using System.ComponentModel.DataAnnotations;
using Dapper.Contrib.Extensions;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace guarderiaApi.models
{
    [Table("Infante")]
    public class Infante
    {
        [Dapper.Contrib.Extensions.Key]
        public int Id { get; set; }
        public string Nombre { get; set; }
        [Required]
        public int TutorId { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Sexo { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Peso { get; set; }
        public string Altura { get; set; }
        public string IndicacionesExtra { get; set; }
        public string Foto { get; set; }
    }
}

