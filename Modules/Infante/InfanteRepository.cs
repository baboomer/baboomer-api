using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using Dapper.Contrib.Extensions;
using guarderiaApi.dto;
using guarderiaApi.models;
using MySql.Data.MySqlClient;

namespace guarderiaApi.repositories
{
    public interface IInfanteRepository
    {
        Task<List<GetInfanteDto>> findAll();
        Task<bool> create(CreateInfanteDto model);
        Task<Infante> findById(string id);
        Task<bool> update(UpdateInfanteDto model);
        Task<bool> delete(int id);
    }

    public class InfanteRepository : IInfanteRepository
    {
        private readonly IDbConnection conn;
        private readonly TutorRepository tutorRepository;

        public InfanteRepository(string connectionString, TutorRepository tutorRepository)
        {
            conn = new MySqlConnection(connectionString);
            this.tutorRepository = tutorRepository;
        }

        public async Task<List<GetInfanteDto>> findAll()
        {
            var sql = "SELECT I.*, T.* FROM Infante I INNER JOIN Tutor T on I.TutorId = T.Id";
            var result = await conn.QueryAsync<GetInfanteDto, GetTutorDto, GetInfanteDto>(sql,
            (i, t) => { i.tutor = t; return i; }, splitOn: "Id");
            return result.AsList();
        }

        public async Task<bool> create(CreateInfanteDto model)
        {
            var tutorExistente = await tutorRepository.findByEmail(model.tutor.Email);
            if(tutorExistente.Count > 0)
            {
                return false;
            }
            await tutorRepository.create(model.tutor);
            var infante = new Infante(){
                Nombre = model.infante.Nombre,
                TutorId = await tutorRepository.getLastId(),
                ApellidoPaterno = model.infante.ApellidoPaterno,
                ApellidoMaterno = model.infante.ApellidoMaterno,
                Sexo = model.infante.Sexo,
                FechaNacimiento = model.infante.FechaNacimiento,
                Peso = model.infante.Peso,
                Altura = model.infante.Altura,
                IndicacionesExtra = model.infante.IndicacionesExtra,
                Foto = model.infante.Foto,
            };
            await conn.InsertAsync(infante);
            return true;
        }

        public async Task<Infante> findById(string id)
        {
            var video = await conn.GetAsync<Infante>(id);
            return video;
        }

        public async Task<bool> update(UpdateInfanteDto model)
        {
            return await conn.UpdateAsync(model);
        }

        public async Task<bool> delete(int id)
        {
            return await conn.DeleteAsync(new Infante { Id = id });
        }
    }
}