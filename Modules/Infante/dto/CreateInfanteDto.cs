using System.ComponentModel.DataAnnotations;
using Dapper.Contrib.Extensions;
using guarderiaApi.models;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace guarderiaApi.dto
{
    public class CreateInfanteDto
    {   
        public Infante infante { get; set; }
        [Required]
        public GetTutorDto tutor { get; set; }
    }
}