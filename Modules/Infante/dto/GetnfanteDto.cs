using System;
using System.ComponentModel.DataAnnotations;
using Dapper.Contrib.Extensions;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace guarderiaApi.dto
{
    public class GetInfanteDto
    {   
        [Required]
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Sexo { get; set; }
        [Required]
        public DateTime FechaNacimiento { get; set; }
        public string Peso { get; set; }
        public string Altura { get; set; }
        public string IndicacionesExtra { get; set; }
        public string Foto { get; set; }
        public GetTutorDto tutor { get; set; }
    }
}