﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirebaseAdmin;
using FirebaseAdmin.Messaging;
using guarderiaApi.dto;
using guarderiaApi.models;
using guarderiaApi.repositories;
using Microsoft.AspNetCore.Mvc;

namespace guarderiaApi.controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TutorController : ControllerBase
    {
        private TutorRepository tutorRepository;

        public TutorController(TutorRepository tutorRepository)
        {
            this.tutorRepository = tutorRepository;
        }

        // GET api/tutor
        [HttpGet]
        public async Task<ActionResult<List<Tutor>>> Get()
        {
            return await tutorRepository.findAll();
        }

        // GET api/tutor/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Tutor>> Get(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return BadRequest();
            }
            var tutor = await tutorRepository.findById(id);
            return tutor;
        }

        // Post api/tutor
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] GetTutorDto tutor)
        {
            var creado = await tutorRepository.create(tutor);
            if (creado)
            {
                return Ok();
            }
            else
            {
                return Conflict();
            }
        }

        // PUT api/tutor/5
        [HttpPut]
        public async Task<ActionResult> Put([FromBody] UpdateTutorDto value)
        {
            var modificado = await tutorRepository.update(value);
            if (modificado)
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }

        // DELETE api/tutor/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var borrado = await tutorRepository.delete(id);
            if (borrado)
            {
                return Ok();
            }
            else
            {
                return Conflict();
            }
        }
        // Post api/tutor/login
        [HttpPost("login")]
        public async Task<ActionResult<Tutor>> Login([FromBody] LoginTutorDto tutor)
        {
            var res = await tutorRepository.login(tutor);
            if(res != null)
            {
                return res;
            }
            return NotFound("Error login");
        }
    }
}
