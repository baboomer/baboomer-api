using System;
using Dapper.Contrib.Extensions;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace guarderiaApi.models
{
    [Table("Tutor")]
    public class Tutor
    {
        [Key]
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Sexo { get; set; }
        public string Email { get; set; }
        public string NumeroTelefono { get; set; }
        public string TokenFirebase { get; set; }
    }
}

