using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using Dapper.Contrib.Extensions;
using guarderiaApi.dto;
using guarderiaApi.models;
using MySql.Data.MySqlClient;

namespace guarderiaApi.repositories
{
    public interface ITutorRepository
    {
        Task<List<Tutor>> findAll();
        Task<bool> create(GetTutorDto model);
        Task<Tutor> findById(string id);
        Task<bool> update(UpdateTutorDto model);
        Task<bool> delete(int id);
    }

    public class TutorRepository : ITutorRepository
    {
        private readonly IDbConnection conn;

        public TutorRepository(string connectionString)
        {
            conn = new MySqlConnection(connectionString);
        }

        public async Task<List<Tutor>> findAll()
        {
            var res = (await conn.GetAllAsync<Tutor>()).AsList();
            return res;
        }

        public async Task<bool> create(GetTutorDto model)
        {
            var id = await conn.InsertAsync(model);
            return true;
        }

        public async Task<Tutor> findById(string id)
        {
            var video = await conn.GetAsync<Tutor>(id);
            return video;
        }

        public async Task<bool> update(UpdateTutorDto model)
        {
            return await conn.UpdateAsync(model);
        }

        public async Task<bool> delete(int id)
        {
            return await conn.DeleteAsync(new Tutor { Id = id });
        }

        public async Task<int> getLastId()
        {
            string query = "SELECT Id from Tutor ORDER BY id DESC LIMIT 1";
            return await conn.QueryFirstAsync<int>(query);
        }
        public async Task<List<Tutor>> findByEmail(string email)
        {
            string sql = $"SELECT * FROM Tutor WHERE Email like '{email}'";
            var res = await conn.QueryAsync<Tutor>(sql);
            return res.AsList();
        }

        public async Task<Tutor> login(LoginTutorDto tutor)
        {
            string UPDATE_TOKEN_FIREBASE = $"UPDATE Tutor SET TokenFirebase='{tutor.TokenFirebase}' WHERE email like '{tutor.Email}';";
            var tutores = await findByEmail(tutor.Email);
            if(tutores.Count > 0){
                await conn.QueryAsync(UPDATE_TOKEN_FIREBASE);
                var tutorRes = tutores[0];
                tutorRes.TokenFirebase = tutor.TokenFirebase;
                return tutores[0];
            }
            return null;
        }
    }
}