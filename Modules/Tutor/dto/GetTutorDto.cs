using System.ComponentModel.DataAnnotations;
using Dapper.Contrib.Extensions;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace guarderiaApi.dto
{
    [Table("Tutor")]
    public class GetTutorDto
    {
        public int Id { get; set; }   
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Sexo { get; set; }
        [Required]
        public string Email { get; set; }
        public string NumeroTelefono { get; set; }
        public string TokenFirebase { get; set; }
    }
}