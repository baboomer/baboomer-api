using System.ComponentModel.DataAnnotations;
using Dapper.Contrib.Extensions;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace guarderiaApi.dto
{
    public class LoginTutorDto
    {   
        [Required]
        public string Email { get; set; }
        public string Password { get; set; }
        [Required]
        public string TokenFirebase { get; set; }
    }
}