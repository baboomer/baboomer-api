﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirebaseAdmin;
using FirebaseAdmin.Messaging;
using guarderiaApi.dto;
using guarderiaApi.models;
using guarderiaApi.repositories;
using Microsoft.AspNetCore.Mvc;

namespace guarderiaApi.controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        private UsuarioRepository usuarioRepository;

        public UsuarioController(UsuarioRepository usuarioRepository)
        {
            this.usuarioRepository = usuarioRepository;
        }

        // GET api/usuario
        [HttpGet]
        public async Task<ActionResult<List<Usuario>>> Get()
        {
            return await usuarioRepository.findAll();
        }

        // GET api/usuario/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Usuario>> Get(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return BadRequest();
            }
            var usuario = await usuarioRepository.findById(id);
            return usuario;
        }

        // Post api/usuario
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateUsuarioDto usuario)
        {
            var buscarUsuario = await usuarioRepository.findByEmail(usuario.Email);
            if(buscarUsuario.Count > 0)
            {
                return Conflict("Ya existe un usuario registrado con ese usuario");
            }
            var creado = await usuarioRepository.create(usuario);
            if (creado)
            {
                return Ok();
            }
            else
            {
                return Conflict();
            }
        }

        // PUT api/usuario/5
        [HttpPut]
        public async Task<ActionResult> Put([FromBody] UpdateUsuarioDto value)
        {
            var modificado = await usuarioRepository.update(value);
            if (modificado)
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }

        // DELETE api/usuario/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var borrado = await usuarioRepository.delete(id);
            if (borrado)
            {
                return Ok();
            }
            else
            {
                return Conflict();
            }
        }

        // Post api/usuario/login
        [HttpPost("login")]
        public async Task<ActionResult<bool>> Login([FromBody] LoginUsuarioDto usuario)
        {
            var res = await usuarioRepository.login(usuario);
            if(res != null)
            {
                return Ok(res);
            }
            return NotFound("Error login");
        }
    }
}
