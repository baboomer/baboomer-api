using System.ComponentModel.DataAnnotations;

namespace guarderiaApi.dto
{
    public class LoginUsuarioDto
    {   
        [Required]
        public string Email { get; set; }
        public string Password { get; set; }
    }
}