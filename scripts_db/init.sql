create table if not exists Usuario
(
    Id int auto_increment primary key,
    TipoUsuario varchar(50)  default '' null,
    Nombre varchar(100) default '' null,
    Email varchar(100) default '' null,
    Password  varchar(100) default '' null,
    UpdatedAt timestamp default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP,
    CreatedAt timestamp default CURRENT_TIMESTAMP null
);

create table if not exists Infante
(
    Id  int auto_increment primary key,
    Nombre varchar(50) default '' not null,
    ApellidoPaterno varchar(100) default '' null,
    ApellidoMaterno varchar(100) default '' null,
    Sexo varchar(100) default '' null,
    FechaNacimiento datetime default CURRENT_TIMESTAMP null,
    Peso varchar(100) default '' null,
    Altura varchar(100) default '' null,
    TutorId int default '' null,
    IndicacionesExtra varchar(100) default '' null,
    Foto LONGTEXT null,
    UpdatedAt timestamp default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP,
    CreatedAt timestamp default CURRENT_TIMESTAMP not null
);

create table if not exists Comunicado
(
    Id  int auto_increment primary key,
    AutorComunicadoId varchar(1000) default '' not null,
    TutorId int null,
    Mensaje varchar(1000) default '' null,
    Imagen LONGTEXT null,
    TipoComunicado int default 0 not null,
    Enterado boolean default 0 not null,
    UpdatedAt timestamp default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP,
    CreatedAt timestamp default CURRENT_TIMESTAMP not null
);

create table if not exists Tutor
(
    Id  int auto_increment primary key,
    Nombre varchar(50) default '' not null,
    ApellidoPaterno varchar(100) default '' null,
    ApellidoMaterno varchar(100) default '' null,
    Sexo varchar(100) default '' null,
    Email varchar(100) default '' null,
    NumeroTelefono varchar(100) default '' null,
    Password varchar(100) default 'contraseña' null,
    TokenFirebase LONGTEXT null,
    UpdatedAt timestamp default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP,
    CreatedAt timestamp default CURRENT_TIMESTAMP not null
);